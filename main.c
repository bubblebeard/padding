
#include <stdio.h>
#include <stdlib.h>

typedef struct bits {
    unsigned bit1 : 1;
    unsigned bit2 : 1;
    unsigned bit3 : 1;
    unsigned bit4 : 1;
    unsigned bit5 : 1;
    unsigned bit6 : 1;
    unsigned bit7 : 1;
    unsigned bit8 : 1;
} bits;

typedef struct chars {
    char a;
    char b;
} chars;

int main(int argc, char** argv) {
    char mas[] = {1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1};
    chars mychar;
    bits mybits = {1, 0, 0, 0, 1, 1, 1, 1};

    printf("bit1 = %d ", mybits.bit1);
    printf("bit2 = %d ", mybits.bit2);
    printf("bit3 = %d ", mybits.bit3);
    printf("bit4 = %d ", mybits.bit4);
    printf("bit5 = %d ", mybits.bit5);
    printf("bit6 = %d ", mybits.bit6);
    printf("bit7 = %d ", mybits.bit7);
    printf("bit8 = %d ", mybits.bit8);
    printf("mychar = %c %c ", mychar.a, mychar.b);
    scanf("%c", mychar.a);
    return (EXIT_SUCCESS);
}

